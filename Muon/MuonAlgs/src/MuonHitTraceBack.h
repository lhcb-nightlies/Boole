#ifndef  MUONALGS_MUONHITTRACEBACK_H
#define  MUONALGS_MUONHITTRACEBACK_H 1

// Include files
#include <algorithm>
#include "Event/MCHit.h"
#include "Event/MCMuonHitHistory.h"
#include "GaudiKernel/SmartRef.h"
#include "MuonHitOrigin.h"

class MuonHitTraceBack final
{
public:
  MuonHitTraceBack();

  void setMCHit(SmartRef<LHCb::MCHit> value);
  const SmartRef<LHCb::MCHit> getMCHit() const;
  SmartRef<LHCb::MCHit> getMCHit();

  void setHitArrivalTime(double value);
  double hitArrivalTime() const;

  void setMCMuonHistory(LHCb::MCMuonHitHistory& value);
  LHCb::MCMuonHitHistory& getMCMuonHistory();

  MuonHitOrigin& getMCMuonHitOrigin();
  void setCordinate(const std::array<float,4>& value) { std::copy(begin(value),end(value),begin(m_cordinate)); }
  const std::array<float,4>&  getCordinate() const { return m_cordinate; }
private:
  SmartRef<LHCb::MCHit> m_MCMuonHit;
  double m_hitArrivalTime ;
  LHCb::MCMuonHitHistory m_history ;
  MuonHitOrigin m_origin ;
  std::array<float,4> m_cordinate ;
};

inline  void MuonHitTraceBack::setMCHit(SmartRef<LHCb::MCHit> value){
  m_MCMuonHit=value ;
}

inline  SmartRef<LHCb::MCHit> MuonHitTraceBack::getMCHit(){
  return m_MCMuonHit ;
}

inline  const SmartRef<LHCb::MCHit> MuonHitTraceBack::getMCHit() const {
  return m_MCMuonHit ;
}

inline void MuonHitTraceBack::setHitArrivalTime(double value){
 m_hitArrivalTime = value ;
}

inline double MuonHitTraceBack::hitArrivalTime() const{
 return m_hitArrivalTime ;
}

inline void MuonHitTraceBack::setMCMuonHistory(LHCb::MCMuonHitHistory&
value){
 m_history=value ;
}

inline LHCb::MCMuonHitHistory& MuonHitTraceBack::getMCMuonHistory(){
  return m_history;
}

inline MuonHitOrigin & MuonHitTraceBack::getMCMuonHitOrigin(){
  return m_origin;
}


#endif
