import argparse
import os, re
import ROOT

def compare(input_detailed_file, input_improved_file, output_directory, histo_name, variable):                                                     

        """Creates a histogram to compare the histograms between the output
	of the detailed and improved simulations."""

        detailed_file = ROOT.TFile(input_detailed_file)
        improved_file = ROOT.TFile(input_improved_file)

	detailed_histo = detailed_file.Get(histo_name)
	improved_histo = improved_file.Get(histo_name)

	detailed_histo.Sumw2()
	detailed_histo.SetLineColor(4)
	detailed_histo.SetLineWidth(2)
	
	improved_histo.Sumw2()
	improved_histo.SetLineColor(2)
	improved_histo.SetLineWidth(2)
	
	c1 = ROOT.TCanvas("c1","Plots",0,0,1200,650)
	
	hs = ROOT.THStack('hs','')
	hs.Add(detailed_histo)
	hs.Add(improved_histo)
	hs.Draw('nostack')
	
	hs.GetXaxis().SetTitle(variable)
	hs.GetXaxis().SetTitleSize(0.05)
	hs.GetXaxis().SetTitleOffset(0.8)
	bin = 700 / 100
	binstr = "%.2f" % bin
	
	hs.GetYaxis().SetTitle('A.U. / ('+binstr+')')
	hs.GetYaxis().SetTitleOffset(1.1)
	hs.GetYaxis().SetTitleSize(0.045)
	
	#Creates the legend                                                                                                                                                                                       
	leg = ROOT.TLegend(0.75,0.75,0.98,0.98)
	leg.SetFillColor(10)
	
	leg.AddEntry(detailed_histo,"Detailed","l")
	leg.AddEntry(improved_histo,"Improved","l")
	leg.Draw('same')
	
	c1.SaveAs(output_directory+'/compare_DetailedImproved_'+variable+'.root')
	c1.SaveAs(output_directory+'/compare_DetailedImproved_'+variable+'.eps')



if __name__ == '__main__':

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument('input_detailed_file', type=str,
                        help='Name of your input detailed ROOT file')
    parser.add_argument('input_improved_file', type=str,
                        help='Name of your input improved ROOT file')
    parser.add_argument('output_directory', type=str,
                        help='Base output name', default = os.getcwd())
    args = parser.parse_args()

    configs = [['MCFTPhotonMonitor/Photons','Photons'],
	       ['MCFTDepositMonitor/ContributionsPerSiPM','ContributionsPerSiPM'],
               ['MCFTDepositMonitor/nDeposits','nDeposits'],
               ['MCFTDepositMonitor/nSignalDeposits','nSignalDeposits'],
               ['MCFTDepositMonitor/nSpilloverDeposits','nSpilloverDeposits'],
               ['MCFTDepositMonitor/nNoiseDeposits','nNoiseDeposits'],
               ['MCFTDepositMonitor/SignalDirect/nPhotonsPerContribution','SignalDirectnPhotonsPerContribution'],
               ['MCFTDepositMonitor/SignalDirect/ArrivalTimePerContribution','SignalDirectnArrivalTimePerContribution'],
               ['MCFTDepositMonitor/SignalDirect/ContributionsPerStation','SignalDirectContributionsPerStation'],
               ['MCFTDepositMonitor/SignalDirect/ContributionsPerModule','SignalDirectContributionsPerModule'],
               ['MCFTDepositMonitor/SignalDirect/ContributionsPerChannel','SignalDirectContributionsPerChannel'],
               ['MCFTDepositMonitor/SignalDirect/ContributionsPerPseudoChannel','SignalDirectContributionsPerPseudoChannel'],
               ['MCFTDepositMonitor/SignalRefl/nPhotonsPerContribution','SignalReflnPhotonsPerContribution'],
               ['MCFTDepositMonitor/SignalRefl/ArrivalTimePerContribution','SignalReflnArrivalTimePerContribution'],
               ['MCFTDepositMonitor/SignalRefl/ContributionsPerStation','SignalReflContributionsPerStation'],
               ['MCFTDepositMonitor/SignalRefl/ContributionsPerModule','SignalReflContributionsPerModule'],
               ['MCFTDepositMonitor/SignalRefl/ContributionsPerChannel','SignalReflContributionsPerChannel'],
               ['MCFTDepositMonitor/SignalRefl/ContributionsPerPseudoChannel','SignalReflContributionsPerPseudoChannel'],
               ['MCFTDepositMonitor/Noise/nPhotonsPerContribution','NoisenPhotonsPerContribution'],
               ['MCFTDepositMonitor/Noise/ArrivalTimePerContribution','NoisenArrivalTimePerContribution'],
               ['MCFTDepositMonitor/Noise/ContributionsPerStation','NoiseContributionsPerStation'],
               ['MCFTDepositMonitor/Noise/ContributionsPerModule','NoiseContributionsPerModule'],
               ['MCFTDepositMonitor/Noise/ContributionsPerChannel','NoiseContributionsPerChannel'],
               ['MCFTDepositMonitor/Noise/ContributionsPerPseudoChannel','NoiseContributionsPerPseudoChannel'],
               ['FTClusterMonitor/Signal/FullClusterCharge','SignalFullClusterCharge'],
	       ['FTClusterMonitor/Signal/FullClusterSize','SignalFullClusterSize'],
               ['FTClusterMonitor/Noise/FullClusterCharge','NoiseFullClusterCharge'],
	       ['FTClusterMonitor/Noise/FullClusterSize','NoiseFullClusterSize']
	       ]

    for config in configs:
        compare(args.input_detailed_file, args.input_improved_file, args.output_directory, config[0], config[1])



