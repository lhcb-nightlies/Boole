import os
if 'PYTHONSTARTUP' in os.environ :
    execfile(os.environ['PYTHONSTARTUP'])
from ROOT import *
from math import *
from array import array
import os, sys
if len(sys.argv) < 2 :
    print "Not enough arguments. Usage: "
    print "   python -i clusterMonitor.py <root-file>"
    print
    sys.exit()
rootFile = sys.argv[1]

outputFile = "plots/PhotonMonitor.pdf"
os.system("mkdir -p plots")

gROOT.ProcessLine(".x /cvmfs/lhcb.cern.ch/lib/lhcb/URANIA/URANIA_v5r0/RootTools/LHCbStyle/src/lhcbStyle.C")
gStyle.SetPalette(1);
gStyle.SetMarkerSize(1.0)
gStyle.SetPadTopMargin(0.07)
gStyle.SetPadLeftMargin(0.20)
gStyle.SetPadRightMargin(0.05)
gStyle.SetTitleYOffset(1.4)
gStyle.SetPalette(kRainBow)
gStyle.SetNumberContours(999)

f = TFile(rootFile)

def histo( name ) :
    h = f.Get( name )
    if h.Class_Name() == "TObject" :
        h = TH1D( )
    return h

c1 = TCanvas("c1","Attenuation map", 800, 300)
c1.Divide(2,1)

c1.cd(1)
c1.GetPad(1).SetRightMargin(0.14)
hAttMapDir = histo("MCFTPhotonMonitor/AttenuationMapDirect")
hAttMapDir.Draw("colz")
latex = TLatex()
latex.DrawLatexNDC(0.2, 0.95, "Direct attenuation map")

c1.cd(2)
c1.GetPad(2).SetRightMargin(0.14)
hAttMapRefl = histo("MCFTPhotonMonitor/AttenuationMapReflected")
hAttMapRefl.Draw("colz")
latex.DrawLatexNDC(0.2, 0.95, "Reflected attenuation map")

c1.Modified()
c1.Update()
c1.SaveAs(outputFile+"(","pdf")

c0 = TCanvas("c0","Attenuation length", 500, 400)

# Define projection function
def projectY( hAttMap, x) :
    xBin = hAttMap.GetXaxis().FindBin(x)
    hAttLen = TH1D(hAttMap.GetName()+"X"+str(xBin),
                   hAttMap.GetTitle()+" projection x="+str(xBin),
                   hAttMap.GetNbinsY(),
                   hAttMap.GetYaxis().GetXmin(),
                   hAttMap.GetYaxis().GetXmax() )

    # fill and invert axis
    for i in range(1, hAttLen.GetNbinsX()+1 ) :
        bin = 1 + hAttLen.GetNbinsX() - i        
        hAttLen.SetBinContent(bin,hAttMap.GetBinContent(xBin,i))
    return hAttLen

leg = TLegend(0.60,0.70,0.90,0.90)
leg.SetMargin(.2);
leg.SetBorderSize(0);
leg.SetTextSize(.07);
leg.SetTextFont(132);
leg.SetLineStyle(0);
leg.SetFillColor(0);
leg.SetFillStyle(0);

hAttLenDirX1 = projectY(hAttMapDir, 130)
hAttLenDirX1.SetMinimum(0.0)
hAttLenDirX1.GetXaxis().SetTitle("Distance to SiPM [mm]")
hAttLenDirX1.GetYaxis().SetTitle("Efficiency")
hAttLenDirX1.SetLineColor(kRed)
hAttLenDirX1.Draw("hist")

hAttLenDirX2 = projectY(hAttMapDir, 3199)
hAttLenDirX2.SetLineColor(kRed+2)
hAttLenDirX2.Draw("hist same")

hAttLenRefX1 = projectY(hAttMapRefl, 130)
hAttLenRefX1.SetLineColor(kBlue)
hAttLenRefX1.Draw("hist same")

hAttLenRefX2 = projectY(hAttMapRefl, 3199)
hAttLenRefX2.SetLineColor(kBlue+2)
hAttLenRefX2.Draw("hist same")

# Fit
decay = TF1("decay","[0]*exp([1]*x)",0,2513)
decay.SetParameter(0,0.025)
decay.SetParameter(1,1./2000.0)
hAttLenDirX1.Fit(decay,"Q0","",25,200)
lambda1 = -1./decay.GetParameter(1) 
hAttLenDirX1.Fit(decay,"Q0","",200,1000)
lambda2 = -1./decay.GetParameter(1) 
hAttLenDirX1.Fit(decay,"Q0","",1000,2200)
lambda3 = -1./decay.GetParameter(1) 
hAttLenDirX1.Fit(decay,"Q0","",2200,2513)
lambda4 = -1./decay.GetParameter(1) 
decay.SetParameters(0.004,-1./3500.0)
hAttLenRefX1.Fit(decay,"Q0","",2200,2513)
lambdaRef1 = 1./decay.GetParameter(1) 
hAttLenRefX1.Fit(decay,"Q0","",0,2200)
lambdaRef2 = 1./decay.GetParameter(1) 
print
print "Attenuation length in range:"
print "  Direct:"
print "    [  25- 200] mm : %4.0f mm" % lambda1
print "    [ 200-1000] mm : %4.0f mm" % lambda2
print "    [1000-2200] mm : %4.0f mm" % lambda3
print "    [2200-2513] mm : %4.0f mm" % lambda4
print " Reflected:"
print "    [2200-2513] mm : %4.0f mm" % lambdaRef1
print "    [   0-2200] mm : %4.0f mm" % lambdaRef2
print

leg1 = leg.Clone("leg1")
leg1.SetX1(0.4)
leg1.AddEntry(hAttLenDirX1, "Direct (x=130mm)", "L")
leg1.AddEntry(hAttLenDirX2, "Direct (x=3200mm)", "L")
leg1.AddEntry(hAttLenRefX1, "Refl.  (x=130mm)", "L")
leg1.AddEntry(hAttLenRefX2, "Refl.  (x=3200mm)", "L")
leg1.Draw()

c0.Modified()
c0.Update()
c0.SaveAs(outputFile,"pdf")

c2 = TCanvas("c2","Photon map", 1200, 430)
c2.SetTopMargin(0.0)
c2.SetLeftMargin(0.0)
c2.SetRightMargin(0.0)
c2.Divide(3,1,0.0, 0.0)
hExitPosMapLeft   = histo("MCFTPhotonMonitor/ExitPosMapLeft")
hExitPosMapMiddle = histo("MCFTPhotonMonitor/ExitPosMapMiddle")
hExitPosMapRight  = histo("MCFTPhotonMonitor/ExitPosMapRight")

box = TBox()
box.SetFillStyle(0)

c2.cd(1)
c2.GetPad(1).SetTopMargin(0.03)
c2.GetPad(1).SetBottomMargin(0.14)
c2.GetPad(1).SetLeftMargin(0.15)
c2.GetPad(1).SetRightMargin(0.01)
hExitPosMapLeft.GetYaxis().SetTitleOffset(1.0)
hExitPosMapLeft.Draw("col")
box.DrawBox(-65.175,-0.65, -64.925, 0.65)
box.DrawBox(-64.925,-0.65, -64.675, 0.65)
box.DrawBox(-64.675,-0.65, -64.425, 0.65)
box.DrawBox(-64.675,-0.65, -64.3,   0.65)


c2.cd(2)
c2.GetPad(2).SetTopMargin(0.03)
c2.GetPad(2).SetBottomMargin(0.14)
c2.GetPad(2).SetLeftMargin(0.08)
c2.GetPad(2).SetRightMargin(0.08)
hExitPosMapMiddle.GetYaxis().SetLabelOffset(2.0)
hExitPosMapMiddle.Draw("col")
box.DrawBox(-0.700,-0.65, -0.475, 0.65)
box.DrawBox(-0.475,-0.65, -0.225, 0.65)
box.DrawBox( 0.475,-0.65,  0.225, 0.65)
box.DrawBox( 0.700,-0.65,  0.475, 0.65)

c2.cd(3)
c2.GetPad(3).SetTopMargin(0.03)
c2.GetPad(3).SetBottomMargin(0.14)
c2.GetPad(3).SetLeftMargin(0.01)
c2.GetPad(3).SetRightMargin(0.15)
hExitPosMapRight.Draw("colz")
box.DrawBox( 65.175,-0.65,  64.925, 0.65)
box.DrawBox( 64.925,-0.65,  64.675, 0.65)
box.DrawBox( 64.675,-0.65,  64.425, 0.65)
box.DrawBox( 64.675,-0.65,  64.3,   0.65)

c2.Modified()
c2.Update()
c2.SaveAs(outputFile,"pdf")

c3 = TCanvas("c3","Photon distributions (1)", 800, 600)
c3.Divide(2,2)
c3.cd(1)
hExitPosX    = histo("MCFTPhotonMonitor/ExitPosX")
hExitPosX.Draw("hist")

c3.cd(2)
hExitPosZ    = histo("MCFTPhotonMonitor/ExitPosZ")
hExitPosZ.Draw("hist")

c3.cd(3)
hdXdY    = histo("MCFTPhotonMonitor/dXdY")
hdXdY.Draw("hist")
hdXdY.Fit("gaus","")

c3.cd(4)
hdZdY    = histo("MCFTPhotonMonitor/dZdY")
hdZdY.Draw("hist")
hdZdY.Fit("gaus","")

c3.Modified()
c3.Update()
c3.SaveAs(outputFile,"pdf")

c4 = TCanvas("c4","Photon distributions (2)", 800, 600)
c4.Divide(2,2)
c4.cd(1)
hnPhotonsPerMat    = histo("MCFTPhotonMonitor/nPhotonsPerMat")
hnPhotonsPerMat.Draw("hist")

c4.cd(2)
hWavelength    = histo("MCFTPhotonMonitor/Wavelength")
hWavelength.Draw("hist")

c4.cd(3)
hPropTimeDirect    = histo("MCFTPhotonMonitor/PropTimeDirect")
hPropTimeReflected = histo("MCFTPhotonMonitor/PropTimeReflected")
hPropTimeDirect.SetLineColor(kGray+2)
hPropTimeReflected.SetLineColor(kGray)
hPropTimeDirect.Draw("hist")
hPropTimeReflected.Draw("hist same")

leg2=leg.Clone("leg2")
leg2.AddEntry(hPropTimeDirect,   "Direct", "L")
leg2.AddEntry(hPropTimeReflected,"Reflected", "L")
leg2.Draw()

c4.cd(4)
hExitTimeDirect    = histo("MCFTPhotonMonitor/ExitTimeDirect")
hExitTimeReflected = histo("MCFTPhotonMonitor/ExitTimeReflected")
hExitTimeDirect.SetLineColor(kGray+2)
hExitTimeReflected.SetLineColor(kGray)
hExitTimeDirect.Draw("hist")
hExitTimeReflected.Draw("hist same")

c4.Modified()
c4.Update()
c4.SaveAs(outputFile+")","pdf")
