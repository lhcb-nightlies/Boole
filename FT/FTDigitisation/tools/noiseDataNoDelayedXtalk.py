# Number of thermal noise clusters w/ 3.3% cross talk
nEvts = 50. # 50=number of evts used for the data (needed for the error)
pXtalk = 0.033
pDelayedXtalk = 0.0
noiseData = []
noiseData.append( [  3.32,   29.46,    3.48,   0.04] ) #  8 MHz
noiseData.append( [ 15.60,  139.28,   23.66,   0.96] ) # 12 MHz
noiseData.append( [ 43.66,  422.96,   93.64,   5.64] ) # 16 MHz
noiseData.append( [ 97.88,  975.68,  267.62,  24.54] ) # 20 MHz
noiseData.append( [191.30, 1869.24,  606.88,  77.12] ) # 24 MHz
noiseData.append( [340.42, 3159.42, 1154.66, 198.96] ) # 28 MHz
