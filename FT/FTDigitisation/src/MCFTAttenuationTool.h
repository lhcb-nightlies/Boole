#ifndef MCFTATTENUATIONTOOL_H
#define MCFTATTENUATIONTOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "IMCFTAttenuationTool.h"            // Interface

/** @class MCFTAttenuationTool MCFTAttenuationTool.h
 *
 *  Tool to calculate the attenuation length for direct and reflectec light in a scintillating fibre.
 *
 *  - ShortAttenuationLength: Distance along the fibre to divide the light amplitude by a factor e : short component
 *  - LongAttenuationLength: Distance along the fibre to divide the light amplitude by a factor e : long component
 *  - FractionShort: Fraction of short attenuation at SiPM
 *  - XMaxIrradiatedZone: Size in x of the zone where fibres are irradiated
 *  - YMaxIrradiatedZone: Size in y of the zone where fibres are irradiated
 *  - IrradiatedAttenuationLength:  Attenuation length by steps in the zone
 *  - ReflectionCoefficient: Reflection coefficient of the fibre mirrored side, from 0 to 1
 *  - BeginReflectionLossY:  Beginning of zone where reflection is too late and lost
 *  - EndReflectionLossY: End of zone where reflection is too late and lost
 *  - XStepOfMap:  Step  along x-axis of the FullAttenuationMap (in mm)
 *  - YStepOfMap:  Step  along y-axis of the FullAttenuationMap (in mm)
 *
 *
 *
 *  @author Michel De Cian, based on Eric Cogneras' implementation
 *  @date   2015-01-15
 */


class MCFTAttenuationTool : public extends<GaudiTool,IMCFTAttenuationTool> {

public:
  using base_class::base_class;

  /// Initialize the transmission map
  StatusCode initialize() override;

  /// Calculate the direct attenuation and the attenuation with reflection
  void attenuation(double x, double y, double& att, double& attRef) override;

private:

  inline double calcAtt(const double fracX, const double fracY, const int kx, const int ky) const{
    return fracX * ( fracY     * m_transmissionMap[m_nYSteps*(kx+1)+ky+1] +
                     ( 1-fracY ) * m_transmissionMap[m_nYSteps*(kx+1)+ky]   ) +
      (1-fracX) * ( fracY     * m_transmissionMap[m_nYSteps*kx+ky+1] +
                    (1-fracY) * m_transmissionMap[m_nYSteps*kx+ky]   );
  }

  inline double calcAttRef(const double fracX, const double fracY, const int kx, const int ky)const{
    return fracX * ( fracY     * m_transmissionRefMap[m_nYSteps*(kx+1)+ky+1] +
                     ( 1-fracY ) * m_transmissionRefMap[m_nYSteps*(kx+1)+ky]   ) +
      (1-fracX) * ( fracY     * m_transmissionRefMap[m_nYSteps*kx+ky+1] +
                    (1-fracY) * m_transmissionRefMap[m_nYSteps*kx+ky]   );
  }

  Gaudi::Property<double> m_shortAttenuationLength{ this,
    "ShortAttenuationLength", 200 * Gaudi::Units::mm,
    "Attenuation length of the light along the fibre: short component"};
  Gaudi::Property<double> m_longAttenuationLength{ this,
    "LongAttenuationLength", 4700 * Gaudi::Units::mm,
    "Attenuation length of the light along the fibre: long component"};
  Gaudi::Property<double> m_fractionShort{ this,
    "FractionShort", 0.18, "Fraction of short attenuation at SiPM"};
  Gaudi::Property<double> m_xMaxIrradiatedZone{ this,
   "XMaxIrradiatedZone", 2000. * Gaudi::Units::mm,
   "Size in x of the zone where fibres are irradiated"};
  Gaudi::Property<double> m_yMaxIrradiatedZone{ this,
   "YMaxIrradiatedZone", 500. * Gaudi::Units::mm,
   "Size in y of the zone where fibres are irradiated"};
  Gaudi::Property<std::vector<double>> m_irradiatedAttenuationLength{ this,
    "IrradiatedAttenuationLength", { 3000. * Gaudi::Units::mm,
        3000. * Gaudi::Units::mm, 3000. * Gaudi::Units::mm,
        1500. * Gaudi::Units::mm, 300. * Gaudi::Units::mm },
    "Attenuation length by steps in the zone"};
  Gaudi::Property<double> m_reflectionCoefficient{ this,
    "ReflectionCoefficient", 0.7,
    "Reflection coefficient of the fibre mirrored side, from 0 to 1"};
  Gaudi::Property<double> m_beginReflectionLossY{ this,
    "BeginReflectionLossY", 1000. * Gaudi::Units::mm,
    "begin of zone where reflection is too late and lost"};
  Gaudi::Property<double> m_endReflectionLossY{ this,
    "EndReflectionLossY", 1500. * Gaudi::Units::mm,
    "end of zone where reflection is too late and lost"};

  Gaudi::Property<double> m_xStepOfMap{ this,
    "XStepOfMap", 200. * Gaudi::Units::mm,
    "Step  along X-axis of the FullAttenuationMap(in mm)"};
  Gaudi::Property<double> m_yStepOfMap{ this,
    "YStepOfMap", 100. * Gaudi::Units::mm,
    "Step  along Y-axis of the FullAttenuationMap(in mm)"};

  Gaudi::Property<double> m_xMax{ this,
      "XMax", 3164.4 * Gaudi::Units::mm,
      "Length of the FullAttenuationMap(in mm) in X-axis"};

  Gaudi::Property<double> m_yMax{ this,
      "YMax", 2426.0 * Gaudi::Units::mm,
      "Length of the FullAttenuationMap(in mm) in Y-axis"};
  
  int          m_nXSteps;
  int          m_nYSteps;
  
  std::vector<double> m_transmissionMap;    ///< Maps hits to transmitted energy from the direct pulse
  std::vector<double> m_transmissionRefMap; ///< Maps hits to transmitted energy from the reflected pulse
};
#endif // MCFTATTENUATIONTOOL_H
