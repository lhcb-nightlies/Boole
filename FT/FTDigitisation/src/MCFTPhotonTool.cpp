/** @file MCFTPhotonTool.cpp
 *
 *  Implementation of class : MCFTPhotonTool
 *
 *  @author Bellee, Violaine and Wishahi, Julian
 *  @date   2017-02-14
 */

// Include files 

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/IRndmEngine.h"

// local
#include "MCFTPhotonTool.h"

// from Event
#include "Event/MCHit.h"

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( MCFTPhotonTool )

//=============================================================================
// Tool initialization
//============================================================================= 
StatusCode MCFTPhotonTool::initialize(){
  auto sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  IRndmGenSvc* randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  if (randSvc == nullptr) {
    return Error( "Failed to access Random Generator Service", StatusCode::FAILURE );
  }

  sc = m_rndmFlat.initialize( randSvc, Rndm::Flat(0.0,1.0) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Flat generator", sc );
  sc = m_rndmGauss.initialize( randSvc, Rndm::Gauss( 0., 1. ) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Gauss generator", sc );

  m_hepRndmEngnIncptn.setGaudiRndmEngine(randSvc->engine());
  m_rndmCLHEPPoisson = std::make_unique<CLHEP::RandPoissonQ>(m_hepRndmEngnIncptn, 1.0);

  return sc;
}
 
//=============================================================================
// Calculate the number of expected photons
//=============================================================================
double MCFTPhotonTool::numExpectedPhotons(double effective_energy){
  return effective_energy * m_photonsPerMeV;
}

//=============================================================================
// Calculate the number of photons
//=============================================================================
int MCFTPhotonTool::numObservedPhotons(double num_expected_photons) {
  // Compute the observed number of photons 
  return m_rndmCLHEPPoisson->fire(num_expected_photons);
}

//=============================================================================
// Randomly generate photon observables
//=============================================================================
void MCFTPhotonTool::generatePhoton(double& time, double& wavelength,
                                    double& posX, double& posZ, 
                                    double& dXdY, double& dZdY) {
  time = generateScintillationTime();
  wavelength = m_wavelengthShift;
  if( m_generateWavelength )
    wavelength += m_wavelengthScale*exp(m_wavelengthShape*m_rndmGauss());
  generatePosAndDir(posX, posZ, dXdY, dZdY);
}

//=============================================================================
// Randomly generate photon exit positon and direction at the fibre end
//=============================================================================
void MCFTPhotonTool::generatePosAndDir(double& posX, double& posZ, 
                                       double& dXdY, double& dZdY) {
  // intermediate photon properties
  //   posR:        r-coordinate of the photon exit point on the fibre-end 
  //                surface (polar coordinates w. (0,0) at the fibre center)
  //   posPhi:      phi-coordinate of the photon exit point on the fibre-end 
  //                surface (polar coordinates w.r.t. z axis in local coo.sys.)
  double posR, posPhi;

  posR = sqrt(m_rndmFlat())*m_effFibreR;
  posPhi = 2.*M_PI*m_rndmFlat();
  posX = posR*cos(posPhi);
  posZ = posR*sin(posPhi);
  
 //Angles
  dXdY = m_rndmGauss()*m_angleVar;
  dZdY = m_rndmGauss()*m_angleVar;
}
