#ifndef SIPMRESPONSE_H
#define SIPMRESPONSE_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiMath/GaudiMath.h"

static const InterfaceID IID_SiPMResponse ( "SiPMResponse", 1, 0 );

/** @class SiPMResponse SiPMResponse.h Boole_v26r9/SiPMResponse.h
 *  
 *  This class describes the SiPM response to a single pulse
 *
 *  @author Maurizio Martinelli
 *  @date   2013-11-12
 */
class SiPMResponse : public GaudiTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_SiPMResponse; }

  /// Standard constructor
  SiPMResponse( const std::string& type, 
                const std::string& name,
                const IInterface* parent);

  virtual ~SiPMResponse( ); ///< Destructor
  
  StatusCode initialize() override;
  
  virtual double response(const double time) const;

protected:

  GaudiMath::Interpolation::Type typeFromString() const;
  void sample(std::vector<double>& times, std::vector<double>& val) const;

private:

  GaudiMath::SimpleSpline* m_responseSpline; 
  double m_tMin;
  double m_tMax;

  // properties
  Gaudi::Property<double> m_samplingDt{ this,
    "samplingDt", 0.1 * Gaudi::Units::ns, "Sampling time step" };
  Gaudi::Property<std::string> m_splineType{ this,
    "splineType", "Cspline", "The spline type" };
  Gaudi::Property<std::string> m_electronicsResponse{ this,
    "ElectronicsResponse", "pacific4",
    "Select which electronics response (pacific) to use" };
  Gaudi::Property<double> m_tshift{ this, "Tshift", 11,
    "Time shift (t0) of the response shape [ns]" };
};
#endif // SIPMRESPONSE_H
