/** @file FTSiPMTool.cpp
 *
 *  Implementation of class : FTSiPMTool
 *
 *  @author Jacco de Vries, Violaine Bellee, and Julian Wishahi
 *  @date   2017-02-14
 */

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// std::partial_sum
#include <numeric>

// from ROOT
#include "Math/ProbFunc.h"
#include "Math/PdfFunc.h"

// local
#include "FTSiPMTool.h"

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( FTSiPMTool )


//=============================================================================
// Tool initialization
//============================================================================= 
StatusCode FTSiPMTool::initialize(){
  auto sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  // Retrieve and initialize DeFT (no test: exception in case of failure)
  m_deFT = getDet<DeFTDetector>( DeFTDetectorLocation::Default );
  if ( m_deFT == nullptr )
    return Error("Could not initialize DeFTDetector.", StatusCode::FAILURE);
  if( m_deFT->version() < 61 )
    return Error("This version requires FTDet v6.1 or higher", StatusCode::FAILURE);

  IRndmGenSvc* randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  if (randSvc == nullptr) {
    return Error( "Failed to access Random Generator Service", StatusCode::FAILURE );
  }

  sc = m_rndmFlat.initialize( randSvc, Rndm::Flat(0.0,1.0) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Flat generator", sc );
  sc = m_rndmLandau.initialize( randSvc, Rndm::Landau(m_meanLandau, m_widthLandau ) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Landau generator", sc );

  m_hepRndmEngnIncptn.setGaudiRndmEngine(randSvc->engine());
  m_rndmCLHEPPoisson = std::make_unique<CLHEP::RandPoissonQ>(m_hepRndmEngnIncptn, 1.0);

  // Set thermal noise probability in 25 ns window
  m_noiseProb = (m_rateThermalNoise/m_readoutFrequency)
       * (m_sipmIrradiation / m_referenceIrradiation)
       * pow(2, (m_sipmTemperature - m_referenceTemperature)/m_temperatureCoefficient);

  // Number of noise deposits over total detector, in m_numNoiseWindows readout windows.
  int nChan = m_deFT->nChannels();
  m_avgNumThermNoiseChans = nChan * m_noiseProb * m_numNoiseWindows;

  // Calculation for the effective noise simulation
  // Integrate Poisson distribution to get probabilities for different thresholds
  std::array<float,3> probOverThreshold;
  for(int iThr=0; iThr< 3 ; ++iThr ){
    const float p = m_effNoiseOverlapProb * m_noiseProb;
    int threshold = 1 + int(m_thresholds[iThr]);
    probOverThreshold[iThr] = ROOT::Math::poisson_cdf_c( threshold-1, p);
    // Add the probabilities in case of additional X-talk photons
    for(int iXtalk=1; iXtalk < threshold ; ++iXtalk) {
      int nDirect = threshold-iXtalk; // number of direct photons
      float totXtalk = m_probDirectXTalk +
          m_probDelayedXTalk*m_delayedXTalkOverlapProb;
      probOverThreshold[iThr] += ROOT::Math::poisson_pdf( nDirect, p) *
          ROOT::Math::binomial_cdf_c(iXtalk-1, totXtalk, nDirect );
    }
  }
  // Probability that left and right neighbours are below lowest threshold
  float pNoNeighbours = std::pow(1.-probOverThreshold[0],2);
  // Calculate approximate probabilities for clusters (depends on clustering algo)
  m_nThermNoiseChansEff[0] = nChan * probOverThreshold[2] * pNoNeighbours;
  m_nThermNoiseChansEff[1] = 2. * nChan * probOverThreshold[1] *
                             probOverThreshold[0] * pNoNeighbours;
  m_nThermNoiseChansEff[2] = nChan * std::pow(probOverThreshold[0],3) * pNoNeighbours ;
  m_nThermNoiseChansEff[3] = nChan * std::pow(probOverThreshold[0],4) ;

  // Number of photons for 1-,2-,3-,4-size clusters (should be > m_thresholds)
  m_nPhotonsPerSize[0] = int(m_thresholds[2]) + 1;
  m_nPhotonsPerSize[1] = int(m_thresholds[1]) + 1;
  m_nPhotonsPerSize[2] = int(m_thresholds[1]) + 1;
  m_nPhotonsPerSize[3] = int(m_thresholds[1]) + 1;

  if( m_simulateEffectiveNoise ) {
    debug() << "Average number of effective thermal noise clusters = "
            << m_nThermNoiseChansEff << endmsg;
  } else {
    debug() << "Average number of thermal noise hits = "
            << m_avgNumThermNoiseChans << endmsg;
  }

  return sc;
}

//=============================================================================
// Add noise to deposit container
//============================================================================= 
void FTSiPMTool::addNoise(LHCb::MCFTDeposits* deposits) {

  // Thermal noise
  if( m_simulateEffectiveNoise ) addThermalNoiseEffective( deposits );
  else addThermalNoise(deposits);

  if ( msgLevel( MSG::DEBUG) )
    debug() << "Number of deposits after adding thermal noise = "
            << deposits->size() << endmsg;

  // After pulse noise
  if( m_probAfterpulse > 0.005 ) addProfileNoise(deposits, m_probAfterpulse);
  if ( msgLevel( MSG::DEBUG) )
    debug() << "Number of deposits after adding afterpulses = "
            << deposits->size() << endmsg;

}

//=============================================================================
// Simulate detailed thermal noise
//============================================================================= 
void FTSiPMTool::addThermalNoise(LHCb::MCFTDeposits* deposits) {
  // Draw poissonian for the number of noise clusters
  int nNoise = m_rndmCLHEPPoisson->fire(m_avgNumThermNoiseChans);
  // Loop over all thermal noise PEs 
  for(int i = 0; i < nNoise; ++i) {
    // Get random channel and time to add 1 PE to
    LHCb::FTChannelID noiseChannel = m_deFT->getRandomChannelFromSeed( m_rndmFlat() );

    // Simulate more than 25ns for the time response
    // and subtract integration time offset per station
    double time = m_rndmFlat() * m_numNoiseWindows * 25.*Gaudi::Units::ns +
        m_integrationOffsets[ noiseChannel.station() - 1 ];

    std::vector<LHCb::MCFTDeposit*> noiseDeps =
        makeNoiseDeposits(noiseChannel, 1, time);
    for( const auto deposit : noiseDeps ) deposits->add(deposit);
  } // end of loop over thermal noise PEs
}

//=============================================================================
// Simulate effective thermal noise
//=============================================================================
void FTSiPMTool::addThermalNoiseEffective(LHCb::MCFTDeposits* deposits) {

  // First add noise to the channels which have signal deposits
  // Sort the deposits, such that deposits for the same channel are neighbours
  std::stable_sort( deposits->begin(), deposits->end(),
                    LHCb::MCFTDeposit::lowerByChannelID );

  // Loop over signal deposits
  std::vector<LHCb::MCFTDeposit*> tmpDeposits;
  LHCb::FTChannelID thisChannel(0);
  for(const auto deposit : *deposits ) {
    if( deposit->channelID() != thisChannel ) {
      if( thisChannel!=0 && deposit->channelID() != thisChannel+1 ) {
        // Generate noise deposits for next channel of previous signal deposit
        generateNoiseDeposits(thisChannel+1, tmpDeposits);
        if( deposit->channelID() != thisChannel+2 ) {
          // Generate noise deposits for previous channel of this signal deposit
          generateNoiseDeposits(deposit->channelID()-1, tmpDeposits);
        }
      }
      thisChannel = deposit->channelID();
      // Generate noise deposits for the current channel
      generateNoiseDeposits(thisChannel, tmpDeposits);
    }
  }
  for( const auto deposit : tmpDeposits ) deposits->add(deposit);

  // Add the thermal noise
  // Loop over the possible cluster size
  for(unsigned int clusSize=1; clusSize<=4; ++clusSize) {
    // Draw poissonian for the number of noise clusters
    int nNoise = m_rndmCLHEPPoisson->fire(m_nThermNoiseChansEff[clusSize-1]);

    // Loop over the number of noise clusters for this cluster size
    for( int j = 0; j < nNoise ; ++j ) {
      LHCb::FTChannelID noiseChannel =
          m_deFT->getRandomChannelFromSeed( m_rndmFlat() );
      double time = m_timeOfNoiseHits +
          m_integrationOffsets[ noiseChannel.station() - 1 ];
      // Create the deposits for this cluster size
      LHCb::FTChannelID firstChannel = noiseChannel-int(0.5*clusSize);
      for( auto iChannel=firstChannel; iChannel-firstChannel < clusSize;
           iChannel.next() ) {
        // Check if the deposit is still in the same module
        if( iChannel.uniqueModule() == noiseChannel.uniqueModule() ) {
          std::vector<LHCb::MCFTDeposit*> noiseDeps =
              makeNoiseDeposits(iChannel, m_nPhotonsPerSize[clusSize-1], time);
          for( const auto deposit : noiseDeps ) deposits->add(deposit);
        }
      }
    }
  }
}


//=============================================================================
// Simulate noise from occupancy profile
//============================================================================= 
void FTSiPMTool::addProfileNoise(LHCb::MCFTDeposits* deposits,
                                 const float scaleFactor) {

  // loop over all 'clusters in the event'
  // multiply amount of generated noise with amount of readout windows to simulate.
  int nClusters = int( m_nClustersInEvent * m_numNoiseWindows * m_scaleLandau);
  for(int i = 0; i < nClusters; ++i) {
    // get number of noise PEs for this cluster
    int nNoisePEs = m_rndmLandau() * scaleFactor;

    // Only generate a noise hit if above threshold
    if( nNoisePEs >= int(m_thresholds[2]) ) { // single cluster threshold
      // For simplicity put all charge in single channel
      auto noiseChannel = generateFromOccupancyProfile();

      // Loop over all photoelectrons
      for( int k=0; k<nNoisePEs; ++k) {
        // Generate a flat time distribution
        double time = m_rndmFlat() * m_numNoiseWindows * 25*Gaudi::Units::ns +
                      m_integrationOffsets.value().at(noiseChannel.station() - 1);
        std::vector<LHCb::MCFTDeposit*> noiseDeps =
            makeNoiseDeposits(noiseChannel, 1, time);
        for( const auto deposit : noiseDeps ) deposits->add(deposit);
      } // end of all photoelectrons
    } 
  } // end loop clusters
}

//=============================================================================
// Randomly generate noise channel from occupancy map
//============================================================================= 
LHCb::FTChannelID FTSiPMTool::generateFromOccupancyProfile() {

  // Generate pseudoChannel according to occupancy profile
  // PDF for occupancy profile (x=pseudochannel)
  //   for x<nCut  :  P(x) = f0/nChan + f1/nCut
  //   for x>=nCut :  P(x) = f0/nChan + (1-f0-f1)*exp(-x/alpha)/normExp
  //          with normExp = alpha * (exp(-nCut/alpha) - exp(-nTot/alpha))
  static const float rMax = exp(-float(m_nCut)/m_alpha);
  static const float rMin = exp(-float(m_nChan)/m_alpha);

  LHCb::FTChannelID noiseChannel(0u);
  while( noiseChannel.channelID() == 0u ) {
    float rndFlat = m_rndmFlat() ; // Draw flat random number [0,1]
    int pseudoChannel = 0;

    // flat overall component
    if( rndFlat < m_f0 ) {
      rndFlat /= m_f0; // renormalize rndFlat in range [0,1]
      pseudoChannel = int(m_nChan * rndFlat); // in range [0,nChan]
    }

    // central flat component
    else if( rndFlat < m_f0.value() + m_f1.value() ) {
      rndFlat = (rndFlat - m_f0) / m_f1; // renormalize rndFlat in range [0,1]
      pseudoChannel = int(m_nCut * rndFlat);  // in range [0,nCut]
    }

    // exponential component
    else {
      // renormalize rndFlat in [rMin, rMax]
      rndFlat = (rndFlat-m_f0-m_f1)/(1.-m_f0-m_f1) * (rMax-rMin)+rMin;
      pseudoChannel = int(-log(rndFlat)*m_alpha); // in range [nCut,nChan]
    }

    noiseChannel = m_deFT->getRandomChannelFromPseudo( pseudoChannel, m_rndmFlat() );
  }

  return noiseChannel;
}

//=============================================================================
// Generate noise deposits for a given channel
//=============================================================================
void FTSiPMTool::generateNoiseDeposits(const LHCb::FTChannelID noiseChannel,
                                       std::vector<LHCb::MCFTDeposit*>& deposits) {
  int nPhotons = m_rndmCLHEPPoisson->fire(m_noiseProb*m_numNoiseWindows);
  for( int i=0; i < nPhotons; ++i ) {
    double time = m_rndmFlat() * m_numNoiseWindows * 25.*Gaudi::Units::ns +
        m_integrationOffsets[ noiseChannel.station() - 1 ];

    std::vector<LHCb::MCFTDeposit*> noiseDeps =
        makeNoiseDeposits(noiseChannel, 1, time);
    for( const auto deposit : noiseDeps ) deposits.push_back(deposit);

  }

}


//=============================================================================
// Add a noise deposit to the deposit container
//============================================================================= 
std::vector<LHCb::MCFTDeposit*> FTSiPMTool::makeNoiseDeposits(
    const LHCb::FTChannelID noiseChannel,
    int nPhotons,
    double time) {

  std::vector<LHCb::MCFTDeposit*> deposits;

  // Create noise deposit with direct cross talk
  LHCb::MCFTDeposit* deposit =  new LHCb::MCFTDeposit(noiseChannel,
      nullptr, nPhotons+generateDirectXTalk(nPhotons), time, false );
  deposits.push_back( deposit );

  // generate delayed XTalk
  int nDelayedXtalk = generateDelayedXTalk(nPhotons);
  for( int i=0; i<nDelayedXtalk; ++i ) {
    LHCb::MCFTDeposit* deposit =  new LHCb::MCFTDeposit(noiseChannel,
        nullptr, 1, time + generateDelayedXTalkTime(), false );
    deposits.push_back( deposit );
  }

  return deposits;
}

//=============================================================================
// Return SiPM PDE
//=============================================================================
float FTSiPMTool::photonDetectionEfficiency(double wavelength){
  float x = wavelength - m_PDEmodelShift;
  return (m_PDEmodelp3*x + m_PDEmodelp2)*x*x + m_PDEmodelp0;
}

//=============================================================================
// Check if photon is detected due to SiPM PDE
//=============================================================================
bool FTSiPMTool::sipmDetectsPhoton(double wavelength){
  // PDE of 2014 SiPM
  return photonDetectionEfficiency(wavelength) > m_rndmFlat();
}
