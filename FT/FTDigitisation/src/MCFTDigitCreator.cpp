//-----------------------------------------------------------------------------
// Implementation file for class : MCFTDigitCreator
//
// 2012-04-04 : COGNERAS Eric
//-----------------------------------------------------------------------------

// local
#include "MCFTDigitCreator.h"

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( MCFTDigitCreator )

MCFTDigitCreator::MCFTDigitCreator(const std::string& name,ISvcLocator* pSvcLocator) :
      Transformer(name, pSvcLocator,
      KeyValue{"InputLocation", LHCb::MCFTDepositLocation::Default},
      KeyValue{"OutputLocation", LHCb::MCFTDigitLocation::Default}) {}


//=============================================================================
// Initialization
//=============================================================================
StatusCode MCFTDigitCreator::initialize() {
  StatusCode sc = Transformer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  

  if ( msgLevel( MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  
  sc = m_gauss.initialize( randSvc(), Rndm::Gauss( 0., 1. ) );
  if ( sc.isFailure() ) return Error( "Failed to get Rndm::Gauss generator", sc );

  // tools
  if ( msgLevel( MSG::DEBUG) ) debug() << ": initializing SiPMResponse" << endmsg;
  m_SiPMResponse = tool<SiPMResponse>("SiPMResponse","SiPMResponse");
  
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::MCFTDigits MCFTDigitCreator::operator()(const LHCb::MCFTDeposits& deposits) const
{
  // Define digits container and register it in the transient data store
  LHCb::MCFTDigits digits;
  digits.reserve(75e3);
  
  // For each FTchannel, converts the deposited photons in ADC count
  double response = 0.;
  std::vector<const LHCb::MCFTDeposit*> contributingDeposits;
  LHCb::MCFTDeposits::const_iterator it = deposits.begin();
  auto nextIt = it+1;
  for( ; it != deposits.end(); ++it, ++nextIt ) {
    const LHCb::MCFTDeposit* deposit = *it;
    if ( msgLevel( MSG::VERBOSE) )
      verbose() << "Make digit from " << deposit->channelID() << endmsg;

    // Calculate the electronic response for this PE
    unsigned int station = deposit -> channelID().station();
    double t = deposit->time() - m_integrationOffset[ station - 1u ];
    response += deposit->nPhotons() *  m_SiPMResponse->response(t);
    contributingDeposits.push_back( deposit );

    // Save the digit when the next deposit is a different channel
    if( nextIt == deposits.end() || deposit->channelID() != (*nextIt)->channelID() ) {

      //== Add noise from the digitisation of the signal
      //   (pedestal subtraction + gain correction of peaks)
      double gain  = 1. * ( 1 + m_gauss() * m_sipmGainVariation );
      double noise = m_adcNoise * m_gauss();
      float PE = response * gain + noise;
      int adc  = ( PE >= m_adcThreshold3 ? 3 :
                   PE >= m_adcThreshold2 ? 2 :
                   PE >= m_adcThreshold1 ? 1 : 0 );

      if( msgLevel( MSG::VERBOSE) )
        verbose() << format("deposit2PhotoElectrons() : Response=%8.3f Gain=%8.3f "
                            "Noise=%4.2f PE = %4f ",response, gain, noise, PE) << endmsg;

      if ( adc > 0 || !m_ignoreZeroADCDigits ) {
        LHCb::MCFTDigit *digit = new LHCb::MCFTDigit( deposit->channelID(), PE, adc,
            contributingDeposits );
        digits.insert(digit);
      }

      // reset
      response = 0.;
      contributingDeposits.clear();
    }
  } // loop on deposits
  
  // Digits are sorted according to their ChannelID to prepare the clusterisation
  std::stable_sort( digits.begin(), digits.end(),
      LHCb::MCFTDigit::lowerByChannelID );

  if ( msgLevel( MSG::DEBUG) )
    debug() << "Number of MCFTDigits created: " << digits.size()<< endmsg;

  return digits;
}
