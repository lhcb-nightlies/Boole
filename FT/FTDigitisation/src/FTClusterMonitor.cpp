// local
#include "FTClusterMonitor.h"

// from boost
#include <boost/math/constants/constants.hpp>

#include "AIDA/IHistogram1D.h"

constexpr static float s_pi = boost::math::constants::pi<float>();

//-----------------------------------------------------------------------------
// Implementation file for class : FTClusterMonitor
//
// 2012-07-05 : Eric Cogneras
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( FTClusterMonitor )

FTClusterMonitor::FTClusterMonitor(const std::string& name,ISvcLocator* pSvcLocator) :
       Consumer(name, pSvcLocator,
       {KeyValue{"LiteClusterLocation", LHCb::FTLiteClusterLocation::Default},
       KeyValue{"FullClusterLocation", LHCb::FTClusterLocation::Default},
       KeyValue{"HitsLocation", LHCb::MCHitLocation::FT},
       KeyValue{"LinkerLocation",
         Links::location(std::string(LHCb::FTLiteClusterLocation::Default)+"2MCHitsWithSpillover")}
       }) {}


//=============================================================================
// Initialization
//=============================================================================
StatusCode FTClusterMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  /// Retrieve and initialize DeFT (no test: exception in case of failure)
  m_deFT = getDet<DeFTDetector>( DeFTDetectorLocation::Default );
  if ( m_deFT == nullptr )
    return Error("Could not initialize DeFTDetector", StatusCode::FAILURE);
  if( m_deFT->version() < 61 )
    return Error("This version requires FTDet v6.1 or higher", StatusCode::FAILURE);

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void FTClusterMonitor::operator()(const FTLiteClusters& clusters,
                                  const LHCb::FTClusters& fullClusters,
                                  const LHCb::MCHits& mcHits,
                                  const LHCb::LinksByKey& links) const {

  // Store unique container of efficient MCHits
  std::set<LHCb::MCHit*> efficientMCHits;

  plot( clusters.size(), "nClusters",
      "Number of clusters; Clusters/event; Events", 0., 15.e3, 100);

  // retrieve FTLiteClustertoMCHitLink
  auto myClusterToHitLink = InputLinks<ContainedObject, LHCb::MCHit>(links);

  uint prevSiPM = 0u, prevModuleID = 0u;
  int clustersInSiPM = 0;

  // Loop over FTCluster
  for ( const auto cluster : clusters ) {
    // Get the FTChannelID
    LHCb::FTChannelID chanID = cluster.channelID();

    // Get all MCHits linked to this (lite)cluster
    const auto mcHitLinks = myClusterToHitLink.from(chanID);

    // Get the correct module
    const DeFTModule* module = m_deFT->findModule( chanID );

    // Get the full cluster (if exists)
    LHCb::FTCluster* fullCluster = (fullClusters.size() > 0 ? fullClusters.object(chanID) : nullptr );

    // Check if cluster is from pure spillover or from pure noise
    bool isSpillover = true;
    for( const auto imcHit : mcHitLinks ) {
      const auto mcHit = imcHit.to();
      if( mcHit->parent()->registry()->identifier() == "/Event/"+LHCb::MCHitLocation::FT ) {
        isSpillover = false;
        // Store this signal MCHit in list of efficient MCHits.
        efficientMCHits.insert( mcHit );
      }
    }
    std::string hitType = "";

    for( auto i : {0, 1} ) {
      if( i == 1 && !isSpillover && !mcHitLinks.empty() ) hitType = "Signal/";
      if( i == 1 && isSpillover ) hitType = "Spillover/";
      if( i == 1 && mcHitLinks.empty() ) hitType = "Noise/";

      // draw cluster channel properties
      plot((float)chanID.station(), hitType+"LiteClustersPerStation",
          "Lite clusters per station; Station; Clusters" , 0.5, 3.5, 3);
      plot((float)chanID.module(), hitType+"LiteClustersPerModule",
          "Lite clusters per module; Module; Clusters" , -0.5, 5.5, 6);
      plot((float)chanID.sipmInModule(), hitType+"LiteClustersPerSiPM",
          "Lite clusters per SiPM; SiPMID; Clusters", 0., 16., 16);
      plot((float)chanID.channel(), hitType+"LiteClustersPerChannel",
          "Lite clusters per channel; Channel; Clusters", -0.5, 127.5, 128);
      plot(cluster.fraction(), hitType+"LiteClusterFraction",
          "Lite cluster fraction; Fraction", -0.25, 0.75, 2);
      plot(cluster.isLarge(), hitType+"LiteClusterIsLarge",
          "Lite cluster size; Size",-0.5, 1.5, 2);

      if( fullCluster ) {
        plot(fullCluster->charge(), hitType+"FullClusterCharge",
            "Full cluster charge; Charge [PE]; Clusters", 0, 100);
        plot(fullCluster->size(), hitType+"FullClusterSize",
            "Full cluster size; Size; Clusters", 0.5, 16.5, 16);
        plot(fullCluster->fraction(), hitType+"FullClusterFraction",
            "Full cluster fraction; Fraction", -0.25, 0.75, 100);
        plot2D(fullCluster->size(), fullCluster->charge(), hitType+"FullClusterChargeVsSize",
            "Full cluster charge vs size; Size; Charge",
            0.5 , 16.5 , 0. , 100., 16, 100);
        plot2D(fullCluster->size(),fullCluster->fraction(), hitType+"FullClusterSizeVsFraction",
            "Full cluster size vs fraction ; Size; Fraction" ,
            0.5 , 16.5, -0.25, 0.75, 16, 100);
      }

      if ( module != nullptr ) {
        int pseudoChannel = module->pseudoChannel( chanID );
        plot(pseudoChannel, hitType+"LiteClustersPerPseudoChannel",
            "Lite clusters per pseudochannel;Pseudochannel;Clusters/(64 channels)",
            0., 12288., 192);
      }
    }

    // Plots for cluster position resolution
    // Get the correct mat
    const DeFTMat* mat = module ? module->findMat( chanID ) : nullptr;
    if ( mat != nullptr ) {
      // Loop over all links to MCHits
      for( const auto imcHit : mcHitLinks ) {
        const auto mcHit = imcHit.to();
        // Plot the resolution only for p > pMin
        if( mcHit->p() < m_minPforResolution ) continue;

        // Plot the resolution only for the signal spill
        if( mcHit->parent()->registry()->identifier() != "/Event/"+LHCb::MCHitLocation::FT ) continue;

        double dXCluster = mat->distancePointToChannel( mcHit->midPoint(),
            chanID, cluster.fraction());
        plot(dXCluster, "Resolution/LiteClusterResolution",
            "Lite cluster resolution; Cluster - MCHit x position [mm]; Number of clusters",-1,1,100);
        plot(dXCluster, "Resolution/LiteClusterResolution"+std::string(cluster.isLarge() ? "Large" : "Small" ),
            "Lite cluster resolution "+std::string(cluster.isLarge() ? "large" : "small" )+" clusters"+
            "; Cluster - MCHit x position [mm]; Number of clusters",-1,1,100);
        if( fullCluster ) {
          plot(dXCluster, "Resolution/LiteClusterResolutionSize"+std::to_string(fullCluster->size()),
              "Lite cluster resolution size"+std::to_string(fullCluster->size())+
              "; Cluster - MCHit x position [mm]; Number of clusters",-1,1,100);

          double dXFullCluster = mat->distancePointToChannel( mcHit->midPoint(),
              chanID, fullCluster->fraction());
          plot(dXFullCluster, "Resolution/FullClusterResolution",
              "Full cluster resolution; Cluster - MCHit x position [mm]; Number of clusters",-1,1,100);
          plot(dXFullCluster, "Resolution/FullClusterResolutionSize"+std::to_string(fullCluster->size()),
              "Full cluster resolution size"+std::to_string(fullCluster->size())+
              "; Cluster - MCHit x position [mm]; Number of clusters",-1,1,100);

          // Plot the angle of the cluster (dx) as function of its size
          Gaudi::XYZPoint localEntry = mat->geometry()->toLocal(mcHit->entry());
          Gaudi::XYZPoint localExit  = mat->geometry()->toLocal(mcHit->exit());
          float tx = (localExit.x() - localEntry.x())/(localExit.z() - localEntry.z());
          float theta = atan(tx)*180/s_pi;
          float ty = (localExit.y() - localEntry.y())/(localExit.z() - localEntry.z());
          float phi = atan(ty)*180/s_pi;
          float dist = (localExit - localEntry).R();
          plot(theta, "Resolution/MCHitTheta",
              "Theta of signal MCHit; #theta(MCHit) [#circ]; Number of clusters",
              -60., 60., 100);
          plot(phi, "Resolution/MCHitPhi",
              "Phi of signal MCHit; #phi(MCHit) [#circ]; Number of clusters",
              -60., 60., 100);
          plot(dist, "Resolution/DistanceInMat",
              "Distance in fibres; #it{x}(MCHit) [mm]; Number of clusters",
              0., 5., 100);
          profile2D(mcHit->entry().x(), mcHit->entry().y(), dist,
              "Resolution/DistanceInMatXYmap",
              "Distance in fibres; #it{x}(MCHit) [mm]; #it{y}(MCHit) [mm]; Distance",
              -3000., 3000., -2500., 2500., 10, 10);
          profile1D(theta, fullCluster->size(), "Resolution/FullClusterSizeVsTheta",
              "Full cluster size vs theta; #theta(MCHit) [#circ]; Cluster size",
              -60., 60., 20);
          std::string thetaCut = std::to_string(10*int(1+std::abs(theta*0.1)));
          plot(dXFullCluster, "Resolution/FullClusterResolutionTheta"+thetaCut,
              "Lite cluster resolution #theta <"+thetaCut+
              "; Cluster - MCHit x position [mm]; Number of clusters",-1,1,100);
        }
      }
    }

    // Count the number of clusters per SiPM
    uint thisSiPM = chanID.uniqueSiPM();
    if( thisSiPM != prevSiPM ) {
      if( clustersInSiPM != 0 ) {
        plot(clustersInSiPM, "LiteClustersInSiPM",
            "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21);
        plot(clustersInSiPM, "LiteClustersInSiPM_Module"+std::to_string(prevModuleID),
            "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21);
        clustersInSiPM = 0;
      }
      prevSiPM = thisSiPM;
      prevModuleID = chanID.module();
    }
    ++clustersInSiPM;
  }

  // Fill this for the last time
  plot(clustersInSiPM, "LiteClustersInSiPM",
      "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21);
  plot(clustersInSiPM, "LiteClustersInSiPM_Module"+std::to_string(prevModuleID),
      "Lite clusters per SiPM; Number of clusters; Number of SiPMs", -0.5, 20.5, 21);

  // Efficiency plots
  int nMCHitsHighP=0, nMCHitsHighPFound=0;
  for( auto const mcHit : mcHits ) {
    bool isEfficient = efficientMCHits.find( mcHit ) != efficientMCHits.end();
    for( auto i : {0, 1} ) {
      if( i == 1 && !isEfficient ) continue; // skip non-efficient hits in 2nd loop
      std::string histName = (i == 1) ? "Found" : "";
      plot(mcHit->energy()/Gaudi::Units::MeV, "HitEfficiency/EnergyLossMCHit"+histName,
          "Energy loss of MCHit; #Delta#it{E} [MeV]; MCHits", 0., 1.0, 100);
      plot(mcHit->time()/Gaudi::Units::ns, "HitEfficiency/TimeMCHit"+histName,
          "Time of flight of MCHit; #it{t}(MCHit) [ns]; MCHits", 20., 80.0, 60);
      if( mcHit->p() > 5*Gaudi::Units::GeV ) {
        if( i == 0 ) nMCHitsHighP++;
        else nMCHitsHighPFound++;
      }
      plot(mcHit->p()/Gaudi::Units::GeV, "HitEfficiency/MomentumMCHit"+histName,
          "Momentum of MCHit; #it{p} [GeV/#it{c}]; MCHits", 0., 20, 80);
    }
  }

  // Print the cluster efficiency for all clusters and for those with p>5 GeV.
  if( mcHits.size() != 0 ) {
    float clusEff      = 100.*float(efficientMCHits.size())/float(mcHits.size()) ;
    float clusEffHighP = (nMCHitsHighP != 0) ?
        100.*float(nMCHitsHighPFound)/float(nMCHitsHighP) : 0;
    if ( msgLevel( MSG::VERBOSE) ) {
      debug() << "Cluster efficiency (> 5 GeV) = " << format("%4.1f", clusEff)
              << "% (" << format("%4.1f", clusEffHighP) << "%)" << endmsg;
    }
    plot(clusEffHighP, "ClustersEfficiency5GeV",
        "Cluster efficiency p>5GeV; Efficiency [%]; Events", 90.1, 100.1, 50);
  }

  return;
}

StatusCode FTClusterMonitor::finalize() {

  AIDA::IHistogram1D* hEff = histo1D( HistoID("ClustersEfficiency5GeV") );
  if( hEff != nullptr ) {
    info() << "Cluster efficiency for p > 5 GeV = "
            << format("%4.1f", hEff->mean()) << "%" << endmsg;
  }

  return Consumer::finalize(); // must be executed last
}





