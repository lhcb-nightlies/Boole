// local
#include "MCFTAttenuationTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MCFTAttenuationTool
//
// 2015-01-15 : Michel De Cian
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( MCFTAttenuationTool )

//=============================================================================
// Calculate the attenuation
//=============================================================================
void MCFTAttenuationTool::attenuation(double hitXPosition, double hitYPosition,
                                      double& att, double& attRef){
  
  //== Compute attenuation by interpolation in the table
  //== No difference made here between x-layers and (u,v)-layers : all layers assumed to be x-layers
  const int kx = std::min(m_nXSteps - 2,(int)(fabs( hitXPosition ) / m_xStepOfMap));
  const int ky = std::min(m_nYSteps - 2,(int)(fabs( hitYPosition ) / m_yStepOfMap));
  const double fracX = fabs( hitXPosition )/m_xStepOfMap - kx;
  const double fracY = fabs( hitYPosition )/m_yStepOfMap - ky;
  
  att    = calcAtt(    fracX, fracY, kx, ky );
  attRef = calcAttRef( fracX, fracY, kx, ky );
}

//=========================================================================
// Calculate the transmission map
//=========================================================================
StatusCode MCFTAttenuationTool::initialize(){
  auto sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  //== Generate the transmission map. 
  //== Two attenuation length, then in the radiation area a different attenuation length
  //== Compute also the transmission of the reflected signal, attenuated by the two length
  //   and the irradiated area

  // Setup maps size
  m_nXSteps = m_xMax / m_xStepOfMap + 2;   // add x=0 and x > max position
  m_nYSteps = m_yMax / m_yStepOfMap + 2;   // same for y
  
  //const double xMax = (m_nXSteps - 1) * m_xStepOfMap;
  const double yMax = (m_nYSteps - 1) * m_yStepOfMap;
  
  // m_transmissionMap set at 1E-10 initialisatino value to avoid 'division by zero' bug
  m_transmissionMap.resize( m_nXSteps * m_nYSteps, 1E-10 );
  m_transmissionRefMap.resize( m_nXSteps * m_nYSteps, 1E-10);
  

  // Loop on the x axis
  for ( int kx = 0; m_nXSteps > kx; ++kx ) {
    const double x = kx * m_xStepOfMap;
    
    // -- is this a model, a fact, an urban legend?
    double radZoneSize = 2 * m_yMaxIrradiatedZone * ( 1 - x / m_xMaxIrradiatedZone );
    if ( 0. > radZoneSize ) radZoneSize = 0.;
    const double yBoundaryRadZone = .5 * radZoneSize;
    

    // Loop on the y axis and calculate transmission map
    for ( int ky = 0; m_nYSteps > ky; ++ky ) {
      const double y = yMax - ky * m_yStepOfMap;

      // Compute attenuation according to the crossed fibre length
      double att = ( m_fractionShort       * exp( -(yMax-y)/m_shortAttenuationLength ) + 
                          ( 1 - m_fractionShort ) * exp( -(yMax-y)/m_longAttenuationLength ) );

      //plot2D(x,y,"FibreAttenuationMap","Attenuation coefficient as a function of the position (Quarter 3); x [mm];y [mm]",
      //       0., m_nXSteps*m_xStepOfMap, 0.,m_nYSteps*m_yStepOfMap, m_nXSteps, m_nYSteps, att);
  
      if ( y < yBoundaryRadZone ){
        att = ( m_fractionShort       * exp( -(yMax-yBoundaryRadZone)/m_shortAttenuationLength ) + 
                ( 1-m_fractionShort ) * exp( -(yMax-yBoundaryRadZone)/m_longAttenuationLength ) );
        
        double lInRadiation = yBoundaryRadZone - y;
        
        for ( unsigned int kz = 0; m_irradiatedAttenuationLength.size() > kz; ++kz ) {
          if ( lInRadiation > m_yStepOfMap ) {
            att  *= exp( - m_yStepOfMap / m_irradiatedAttenuationLength[kz] );
          } else if ( lInRadiation > 0. ) {
            att  *= exp( - lInRadiation / m_irradiatedAttenuationLength[kz] );
          } 
          lInRadiation -= m_yStepOfMap;
        }
        
      }
      m_transmissionMap[ m_nYSteps * (kx+1) - ky - 1 ] = att;
    }
    
    // Loop on the y axis and calculate transmission of the reflected pulse
    // The energy from the reflected pulse is calculated as the energy coming from y = 0 and attenuated 
    // by the reflection coefficient, further attenuated depending on the y.
    if ( m_reflectionCoefficient > 0. ) {

      // Reflected energy from y = 0
      double reflected = m_transmissionMap[ m_nYSteps * kx ] * m_reflectionCoefficient;
      
      for ( int kk = 0; m_nYSteps > kk; ++kk ) {
        //const double y = kk * m_yStepOfMap;

        // Evaluate the attenuation factor. 
        // Rather than calculating it use information from the transmission map in the same point
        double att = 0;
     
        if (m_nYSteps > (kk + 1)) att = m_transmissionMap[ m_nYSteps*kx + kk] / m_transmissionMap[ m_nYSteps*kx + kk + 1];

        // Fill the map
        m_transmissionRefMap[ m_nYSteps*kx + kk] = reflected;

        // Prepare the reflection ratio for next iteration
        reflected *= att;
      }
    }
  }
  return sc;
}

