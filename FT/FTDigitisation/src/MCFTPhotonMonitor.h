#ifndef MCFTPHOTONMONITOR_H 
#define MCFTPHOTONMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Consumer.h"

// from Event
#include "Event/MCHit.h"

// from FTEvent
#include "Event/MCFTPhoton.h"

/** @class MCFTPhotonMonitor MCFTPhotonMonitor.h
 *  
 *
 *  @author Violaine Bellee, Julian Wishahi, Luca Pescatore
 *  @date   2016-12-18
 */
class MCFTPhotonMonitor : public Gaudi::Functional::Consumer
                          <void(const LHCb::MCFTPhotons&),
                          Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

public: 

  //using GaudiHistoAlg::GaudiHistoAlg;
  MCFTPhotonMonitor(const std::string& name,ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  void operator()(const LHCb::MCFTPhotons& deposits) const override;
  
private:
  //Gaudi properties
 
  // AttenuatioTool to plot the attenuation maps
  Gaudi::Property<std::string> m_attenuationToolName{this, "AttenuationToolName",
    "MCFTG4AttenuationTool","Name of the MCFTAttenuationTool"};

};
#endif // MCFTPHOTONMONITOR_H
